import requests
from twilio.rest import Client
STOCK = "TSLA"
COMPANY_NAME = "Tesla Inc"
STOCK_KEY = "YOUR_API_KEY_FOR_ALPHAVANTAGE"
NEWS_KEY = "YOUR_API_KEY_FOR_NEWS_API"

account_sid = "YOUR_TWILIO_SID"
auth_token = "YOUR_TWILIO_AUTH_TOKEN"


def get_stock_data():
    url = "https://www.alphavantage.co/query"
    parameters = {
        # "function": "TIME_SERIES_DAILY",
        "function": "GLOBAL_QUOTE",
        "symbol": STOCK,
        "apikey": STOCK_KEY
    }
    response = requests.get(url, parameters)
    response.raise_for_status()
    data = response.json()["Global Quote"]
    if float(data["05. price"]) > float(data["08. previous close"]):
        sign = "🔺"
    else:
        sign = "🔻"
    stock = {
        "current_price": data["05. price"],
        "previous_price": data["08. previous close"],
        "change":  float(data["10. change percent"].strip("%")),
        "sign": sign
    }
    return stock


def get_news():
    url = "https://newsapi.org/v2/everything"
    parameters = {
        "q": COMPANY_NAME,
        "pageSize": 3,
        "apikey": NEWS_KEY
    }
    response = requests.get(url, parameters)
    response.raise_for_status()
    news = response.json()["articles"]
    return news


def send_message(message_body):
    client = Client(account_sid, auth_token)
    message = client.messages \
                    .create(
                        body=message_body,
                        from_='+16507970322',
                        to='+country_code_and_phone_no'
                    )
    print(message.status)


stock_data = get_stock_data()

if stock_data["change"] >= 5:
    news_list = get_news()
    message = f"""
{STOCK}: {stock_data["sign"]} {stock_data["change"]}%
Headline: {news_list[0]["title"]}
Brief: {news_list[0]["description"]}
----------------------------------------------------
Headline: {news_list[1]["title"]}
Brief: {news_list[1]["description"]}
----------------------------------------------------
Headline: {news_list[1]["title"]}
Brief: {news_list[1]["description"]}
    """
    send_message(message)
